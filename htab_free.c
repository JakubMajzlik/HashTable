/*
htab_free.c
Riesenie IJC-DU2
Autor: Jakub Majzlik, FIT
Login: xmajzl02
Datum: 18.4.2017
Prekladac: GCC 5.3.0
*/
#include "htab.h"
/*Funkcia, ktora uvolni naalokovanu pamet*/
void htab_free(htab_t *t)
{
    if(t == NULL)
    {
        return;
    }

    htab_clear(t);

    free(t);

    t = NULL;
}
