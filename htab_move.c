/*
htab_move.c
Riesenie IJC-DU2
Autor: Jakub Majzlik, FIT
Login: xmajzl02
Datum: 17.4.2017
Prekladac: GCC 5.3.0
*/
#include "htab.h"
/*Funkcia, ktara inicializuje novu tabulku a presunie prvky zo starej tabulky*/
htab_t *htab_move(unsigned int newsize, htab_t *t2)
{
    if(t2 == NULL)
    {
        return 0;
    }

    htab_t *table = htab_init(newsize);

    if(!table)
    {
        return NULL;
    }

    for(unsigned int i = 0; i < t2->arr_size; i++)
    {
        if(i<newsize)
        {
            table->ptr[i] = t2->ptr[i];
            t2->ptr[i] = NULL;
        }
        else
        {
            t2->ptr[i] = NULL;
        }
    }

    return table;
}
