/*
htab_bucket_count.c
Riesenie IJC-DU2
Autor: Jakub Majzlik, FIT
Login: xmajzl02
Datum: 15.4.2017
Prekladac: GCC 5.3.0
*/
#include "htab.h"
/*Funkcia, ktora vrati pocet riadkov tabulky*/
size_t htab_bucket_count(htab_t *t)
{
    if(t == NULL)
    {
        return 0;
    }
    return t->arr_size;
}
