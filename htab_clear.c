/*
htab_free.c
Riesenie IJC-DU2
Autor: Jakub Majzlik, FIT
Login: xmajzl02
Datum: 18.4.2017
Prekladac: GCC 5.3.0
*/
#include "htab.h"

/*Funkcia ktora vycisti tabulku*/
void htab_clear(htab_t *t)
{
    if(t == NULL)
    {
        return;
    }

    struct htab_listitem *item = NULL;
    struct htab_listitem *temp = NULL;
    for(unsigned int i = 0; i < t->arr_size; i++)
    {
        if(t->ptr[i] == NULL)
        {
            continue;
        }

        item = t->ptr[i];

        for (; item != NULL; item = temp)
        {
            temp = item->next;
            htab_remove(t, item->key);
        }
        t->ptr[i] = NULL;
    }

}
