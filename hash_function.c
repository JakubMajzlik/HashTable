/*
hash_function.c
Riesenie IJC-DU2
Autor: Jakub Majzlik, FIT
Login: xmajzl02
Datum: 14.4.2017
Prekladac: GCC 5.3.0
*/
#include "htab.h"
/*Hashovacia funkcia*/
unsigned int hash_function(char *str)
{
    unsigned int h=0;
    const unsigned char *p;

    for(p=(const unsigned char*)str; *p!='\0'; p++)
    {
        h = 65599*h + *p;
    }
    return h;
}
