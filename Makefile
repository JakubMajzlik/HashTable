#
#Makefile
#Riesenie IJC-DU1
#Autor: Jakub Majzlik, FIT
#Makefile
#Riesenie IJC-DU2
#Autor: Jakub Majzlik, FIT
#Login: xmajzl02
#Datum: 14.4.2017
#Prekladac: GCC 5.3.0
##############

all: tail tail2 wordcount wordcount-dynamic libhtab.a libhtab.so

tail: tail.o
	gcc -std=c99 -pedantic -Wall -Wextra -o2 tail.o -o tail
tail2:tail2.cc
	g++ -std=c++11 -pedantic -Wall tail2.cc -o tail2
wordcount: wordcount_c.o libhtab.a io.o
	gcc -std=c99 -pedantic -Wall -Wextra -o2 wordcount_c.o io.o libhtab.a -static -o wordcount
wordcount-dynamic: wordcount.o io.o libhtab.so
	gcc -std=c99 -pedantic -Wall -Wextra -o2 -L. wordcount_c.o io.o libhtab.so -o wordcount-dynamic -lhtab
###############
tail.o: tail.c
	gcc -std=c99 -pedantic -Wall -Wextra -o2 -c tail.c -o tail.o
wordcount_c.o:wordcount.c
	gcc -std=c99 -pedantic -Wall -Wextra -o2 -c wordcount.c -o wordcount_c.o
##############
htab_init.o: htab_init.c
	gcc -std=c99 -pedantic -Wall -Wextra -o2 -fPIC -c htab_init.c -o htab_init.o
htab_find.o: htab_find.c
	gcc -std=c99 -pedantic -Wall -Wextra -o2 -fPIC -c htab_find.c -o htab_find.o
htab_clear.o: htab_clear.c
	gcc -std=c99 -pedantic -Wall -Wextra -o2 -fPIC -c htab_clear.c -o htab_clear.o
htab_remove.o: htab_remove.c
	gcc -std=c99 -pedantic -Wall -Wextra -o2 -fPIC -c htab_remove.c -o htab_remove.o
htab_free.o: htab_free.c
	gcc -std=c99 -pedantic -Wall -Wextra -o2 -fPIC -c htab_free.c -o htab_free.o
htab_foreach.o: htab_foreach.c
	gcc -std=c99 -pedantic -Wall -Wextra -o2 -fPIC -c htab_foreach.c -o htab_foreach.o
htab_bucket_count.o: htab_bucket_count.c
	gcc -std=c99 -pedantic -Wall -Wextra -o2 -fPIC -c htab_bucket_count.c -o htab_bucket_count.o
htab_lookup_add.o: htab_lookup_add.c
	gcc -std=c99 -pedantic -Wall -Wextra -o2 -fPIC -c htab_lookup_add.c -o htab_lookup_add.o
hash_function.o: hash_function.c
	gcc -std=c99 -pedantic -Wall -Wextra -o2 -fPIC -c hash_function.c -o hash_function.o
htab_move.o: htab_move.c
	gcc -std=c99 -pedantic -Wall -Wextra -o2 -fPIC -c htab_move.c -o htab_move.o
htab_size.o: htab_size.c
	gcc -std=c99 -pedantic -Wall -Wextra -o2 -fPIC -c htab_size.c -o htab_size.o
#############
io.o: io.c
	gcc -std=c99 -pedantic -Wall -Wextra -o2 -c io.c -o io.o

##############
libhtab.a: htab_init.o htab_lookup_add.o htab_find.o htab_foreach.o hash_function.o htab_clear.o htab_free.o htab_remove.o htab_bucket_count.o htab_move.o htab_size.o
	ar rcs libhtab.a htab_init.o htab_lookup_add.o htab_find.o htab_foreach.o hash_function.o htab_clear.o htab_free.o htab_remove.o htab_bucket_count.o htab_move.o htab_size.o
libhtab.so: htab_init.o htab_lookup_add.o htab_find.o htab_foreach.o hash_function.o htab_clear.o htab_free.o htab_remove.o htab_bucket_count.o htab_move.o htab_size.o
	gcc -std=c99 -pedantic -Wall -Wextra -o2 -shared -fPIC htab_init.o htab_lookup_add.o htab_find.o htab_foreach.o hash_function.o htab_clear.o htab_free.o htab_remove.o htab_bucket_count.o htab_move.o htab_size.o -o libhtab.so
clean:
	$(RM) -r *.o libhtab.a libhtab.so tail tail2 wordcount wordcount-dynamic
