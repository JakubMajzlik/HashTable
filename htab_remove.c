/*
htab_remove.c
Riesenie IJC-DU2
Autor: Jakub Majzlik, FIT
Login: xmajzl02
Datum: 17.4.2017
Prekladac: GCC 5.3.0
*/
#include "htab.h"
/*Funkcia, ktora odstrani prvok so specifickym menom*/
bool htab_remove(htab_t *t, char *key)
{
    if(t == NULL)
    {
        return false;
    }

    unsigned int i = hash_function(key) % htab_bucket_count(t);

    struct htab_listitem *temp = t->ptr[i];
    /* Nasledujuca polozka */
    struct htab_listitem *temp2 = NULL;
    /* Predchadzajuca polozka */
    struct htab_listitem *temp3 = NULL;

    for(; temp != NULL; temp = temp2)
    {
        temp2 = temp->next;
        if(strcmp(temp->key, key) == 0)
        {
            t->n--;
            free(temp->key);
            if(temp3 == NULL)
            {
                if(temp2 == NULL)
                {
                    free(temp);
                    return true;
                }
                else
                {
                    t->ptr[i] = temp2;
                    free(temp);
                    return true;
                }
            }
            else
            {
                if(temp2 == NULL)
                {
                    free(temp);
                    return true;
                }
                else
                {
                    temp3->next = temp2;
                    free(temp);
                    return true;
                }
            }
        }
        temp3=temp;
    }
    return false;
}
