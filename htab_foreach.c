/*
htab_foreach.c
Riesenie IJC-DU2
Autor: Jakub Majzlik, FIT
Login: xmajzl02
Datum: 25.4.2017
Prekladac: GCC 5.3.0
*/
#include "htab.h"
/*Funkcia, ktora vykona funkciu danu parametrom na vsetky prvky tabulky*/
void htab_foreach(htab_t *t, void (*func)(char *, unsigned int))
{
    if(t == NULL)
    {
        return;
    }

    for(unsigned int i = 0; i < t->arr_size; i++)
    {
        struct htab_listitem *item = t->ptr[i];
        for(; item != NULL; item = item->next)
        {
            func(item->key, item->data);
        }
    }



}
