/*
htab.h
Riesenie IJC-DU2
Autor: Jakub Majzlik, FIT
Login: xmajzl02
Datum: 16.4.2017
Prekladac: GCC 5.3.0
*/
 #ifndef __HTAB_H__
    #define __HTAB_H__

#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

typedef struct
{
    unsigned int arr_size;
    unsigned int n;
    struct htab_listitem *ptr[];
}htab_t;

struct htab_listitem
{
    char *key;
    unsigned int data;
    struct htab_listitem *next;
};

htab_t *htab_init(unsigned int size);

htab_t *htab_move(unsigned int newsize, htab_t *t2);

size_t htab_size(htab_t *t);

size_t htab_bucket_count(htab_t *t);

bool htab_remove(htab_t *t, char *key);

unsigned int hash_function(char *str);

struct htab_listitem *htab_lookup_add(htab_t *t, char *key);

struct htab_listitem *htab_find(htab_t *t, char *key);

void htab_foreach(htab_t *t, void (*func)(char *, unsigned int));

void htab_clear(htab_t *t);

void htab_free(htab_t *t);

#endif
