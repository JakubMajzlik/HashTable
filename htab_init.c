/*
htab_init.c
Riesenie IJC-DU2
Autor: Jakub Majzlik, FIT
Login: xmajzl02
Datum: 15.4.2017
Prekladac: GCC 5.3.0
*/
#include "htab.h"
/*Funkcia na inicializovanie tabulky*/
htab_t *htab_init(unsigned int size)
{
    if(size == 0)
    {
        return NULL;
    }

    htab_t *table = (htab_t *) malloc(sizeof(htab_t) + size * sizeof(struct htab_listitem *));

    if(!table)
    {
        return NULL;
    }

    table->arr_size = size;
    table->n = 0;

    for(unsigned int i = 0; i < size; i++)
    {
        table->ptr[i] = NULL;
    }

    return table;
}
