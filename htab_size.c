/*
htab_size.c
Riesenie IJC-DU2
Autor: Jakub Majzlik, FIT
Login: xmajzl02
Datum: 18.4.2017
Prekladac: GCC 5.3.0
*/
#include "htab.h"
/*Funkcia, ktora vrati pocet prvkov v tabulke*/
size_t htab_size(htab_t *t)
{
    if(t == NULL)
    {
        return 0;
    }

    return t->n;
}
