/*
htab_lookup_add.c
Riesenie IJC-DU2
Autor: Jakub Majzlik, FIT
Login: xmajzl02
Datum: 16.4.2017
Prekladac: GCC 5.3.0
*/
#include "htab.h"
/*Funkcia na vyhladanie prvku v tabulke, ak nenajde vytvori novy prvok*/
struct htab_listitem *htab_lookup_add(htab_t *t, char *key)
{
    if(t == NULL)
    {
        return NULL;
    }

    unsigned int i = hash_function(key) % htab_bucket_count(t);
    struct htab_listitem *temp = t->ptr[i];
    struct htab_listitem *temp2 = NULL;
    for(;temp != NULL; temp = temp->next)
    {
        if(strcmp(temp->key, key) == 0)
        {
            return temp;
        }
        temp2 = temp;
    }
    struct htab_listitem *item = (struct htab_listitem *) malloc(sizeof(struct htab_listitem));

    if(!item)
    {
        return NULL;
    }

    item->key = (char *) malloc((strlen(key) + 1 ) * sizeof(char));

    if(!item->key)
    {
        free(item);
        return NULL;
    }

    strcpy(item->key, key);
    item->data = 0;
    item->next = NULL;

    if(temp2 == NULL)
    {
        t->ptr[i] = item;
    }
    else
    {
        temp2->next = item;
    }

    t->n++;

    return item;
}
