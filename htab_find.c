/*
htab_find.c
Riesenie IJC-DU2
Autor: Jakub Majzlik, FIT
Login: xmajzl02
Datum: 16.4.2017
Prekladac: GCC 5.3.0
*/
#include "htab.h"

/**
* Funkcia na vyhladanie prvku v tabulke, ak nenajde vrati NULL. Ak tabulka 
* nieje inicializovana, tak vrati NULL tiez.
* @param *t Tabulka, v ktorej sa ma hladat item s identifikatorom *key
* @param *key Identifikator itemu, ktory sa ma hladat v tabulke *t 
* @return Ukayovatel na item v tabulke *t
*/
struct htab_listitem *htab_find(htab_t *t, char *key)
{
    if(t == NULL)
    {
        return NULL;
    }

    unsigned int i = hash_function(key) % htab_bucket_count(t);
    struct htab_listitem *temp = t->ptr[i];

    while(temp != NULL)
    {
        if(temp->key == key)
        {
            return temp;
        }
        temp = temp->next;
    }
    return NULL;
}
